#include "sorter.h"

// The Editors Dream
// "Test 2"
// By Jeffrey Thompson and Kenley Arai

using namespace std;

int main()
{
	while(1)
	{
		double time = 0;
		int i =0;
		while(1)
		{
			Sorter S("war_and_peace.txt");
			S.process();
			time += S.iTime;
			cout<<S;
			++i;
			cout<<"Average time over "<<i<<" runs: "<<time/i<<endl;
			//S.menu();
		}
	}
}
