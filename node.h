#ifndef NODE_H
#define NODE_H

#include "include.h"

struct Node
{
		std::string word;
		std::vector<int> location;

		Node(){}
		~Node(){}
		Node(Node const &copyMine) : word(copyMine.word), location(copyMine.location)
		{}
		Node(const std::string &takeMe, int par, int sen) : word(takeMe), location(2, par)
		{
			location[1]=sen;
		}
		void operator = (const Node &copyMe)
		{
			word = copyMe.word;
			location = copyMe.location;
		}
		void operator += (const Node &addMine)
		{
			location.push_back(addMine.location[0]);
			location.push_back(addMine.location[1]);
		}
		unsigned int first()const
		{
			return (word[0] - 'A');  //get the number of the first - 'A' = 0, 'Z' = 25
		}
		int count()const
		{
			return (location.size()/2); //as even's are paragraphs, odds are sentances
		}
		friend
		std::ostream& operator <<(std::ostream &out, const Node &n)  //this is a mess...
		{
			out<<n.word<<" found at "<<n.location[0];
			for(unsigned int i = 1, top = n.location.size(); i < top; ++i)
			{
				if(!(i%10))
					out<<std::endl;
				if(!(i%2))
					out<<" & ";
				else
					out<<"|";
				out<<n.location[i];
			}
			out<<std::endl;
			return out;
		}
		friend
		std::istream& operator >>(std::istream &in, Node &n)
		{
			n.word.clear();
			n.location.clear();
			in>>n.word;
			return in;    //incomplete but we don't actually use this
		}
		friend
		bool operator == (const Node &l, const Node &r)
		{
			return l.word == r.word;
		}
		friend
		bool operator < (const Node &l, const Node &r)
		{
			return l.word < r.word;
		}
};


#endif // NODE_H
