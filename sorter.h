#ifndef SORTER_H
#define SORTER_H

#include "include.h"
#include "parser.h"
#include "heap.h"
#include "node.h"


// toggles for different modes

//#define USING_HEAP


class Sorter
{
	public:
		Sorter();
		Sorter(const char *fileName);
		~Sorter();

		void open(char *fileName);
        void process();
        void menu();
		void PrintWords(std::ostream & out);

		friend
        std::ostream& operator << (std::ostream& out, const Sorter & S);

		timer iTime;  //making this one public
	private:
		void initialize();
		void checkTopTen (Node* checkThis);
		//lots of variables...
		Node* topTen[10];  //top ten used words
		Parser P;   //our parser
		std::vector<Node*> sorted;  //a single list of all Nodes in alphabetical order
		timer rTime;
		int para, sent, syla, total, totUnique, unique[26];  //counts of stuff
#ifdef USING_HEAP
		heap<Node> box[26];
#else
		binaryTree<Node> box[26];
#endif
};


#endif // SORTER_H
